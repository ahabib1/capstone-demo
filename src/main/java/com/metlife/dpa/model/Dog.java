package com.metlife.dpa.model;

public class Dog {
	String name;
	String age;
	String breed;
	
	public Dog(String name, String age, String breed){
		this.name = name;
		this.age = age;
		this.breed= breed;
	}
	public Dog(){}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getBreed() {
		return breed;
	}
	public void setBreed(String breed) {
		this.breed = breed;
	}
}
