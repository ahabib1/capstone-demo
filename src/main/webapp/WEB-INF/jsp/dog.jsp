<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/CapstoneDemo/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/CapstoneDemo/bootstrap/mycss.css" rel="stylesheet" type="text/css" />


<title>Insert title here</title>

</head>
<body>
<div class="container">
<ul class="nav nav-tabs">
  <li><a href="/CapstoneDemo/app/">Home</a></li>
  <li class="active"><a href="dog">Dog</a></li>
  <li><a href="greeting">Greeting</a></li>
</ul>
<form:form commandName="askDog">
   <table>
       <tr>
        <td><form:label path="name">Cat Name:</form:label></td>
        <td><form:input path="name" /> </td>
    </tr>
    <tr>
        <td><form:label path="breed">Cat Breed:</form:label></td>
        <td><form:input path="breed" /> </td>
    </tr>
        <tr>
        <td><form:label path="age">Cat Age (in yrs):</form:label></td>
        <td><form:input path="age" /> </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form:form>
</div>
</body>
</html>