<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<link href="/CapstoneDemo/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/CapstoneDemo/bootstrap/mycss.css" rel="stylesheet" type="text/css" />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div class="container">
<ul class="nav nav-tabs">
  <li><a href="/CapstoneDemo/app/">Home</a></li>
  <li><a href="dog">Dog</a></li>
  <li  class="active"><a href="greeting">Greeting</a></li>
</ul>
<form:form commandName="formGreeting">
   <table>
    <tr>
        <td><form:label path="content">Content</form:label></td>
        <td><form:input path="content" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
    </tr>
</table>  
</form:form>
</div>
</body>
</html>