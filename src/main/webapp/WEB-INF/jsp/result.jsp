<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/CapstoneDemo/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/CapstoneDemo/bootstrap/mycss.css" rel="stylesheet" type="text/css" />
<title>Insert title here</title>
</head>
<body>
<div class="container">
<ul class="nav nav-tabs">
  <li><a href="/CapstoneDemo/app/">Home</a></li>
  <li><a href="dog">Dog</a></li>
  <li  class="active"><a href="greeting">Greeting</a></li>
</ul>
The user wrote ${ greeting.content }
</div>
</body>
</html>