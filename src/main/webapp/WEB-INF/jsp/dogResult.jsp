<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/CapstoneDemo/bootstrap/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/CapstoneDemo/bootstrap/mycss.css" rel="stylesheet" type="text/css" />


<title>Insert title here</title>
</head>
<body>
<div class="container">
<ul class="nav nav-tabs">
  <li><a href="/CapstoneDemo/app/">Home</a></li>
  <li class="active"><a href="dog">Dog</a></li>
  <li><a href="greeting">Greeting</a></li>
</ul>
The cat's name is ${dog.name } it is a ${dog.breed } that is ${dog.age} years old
</div>
</body>
</html>